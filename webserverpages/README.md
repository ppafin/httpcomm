# webserver

Apache2 php pages to support httpcomm

1. Create directories on apache2 server

```
/var/www/html/uploads
/var/www/html/up
```

2. Place *.php to /var/www/html/up/

3. Configure config.ini

```
# Server URL's
deleteurl = https://[server_url]/up/d.php?file=
listfilesurl = https://[server_url]/up/l.php
downloadfileurl=https://[server_url]/uploads
uploadfileurl=https://[server_url]/up/upload.php
```

TODO: 

* Input validation and other checks!



