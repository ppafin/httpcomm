CC=gcc

EXTRA_WARNINGS = -Wall 

LIBS = -lcurl

CFLAGS=-ggdb $(EXTRA_WARNINGS)

BINS=httpcomm

all: httpcomm

httpcomm:	main.c log.c ini.c
	 $(CC) $+ $(CFLAGS) $(LIBS) -o $@ -I.

	 
clean:
	rm -rf $(BINS)
	rm *.o
	
