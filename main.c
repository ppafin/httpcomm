 /* 
  * httpcomm - http file upload and download with curl libary
  * Copyright (C) 2021 Pasi Patama, <pasi@patama.net>
  * 
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  * 
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
  *  
  * Based on curl examples by: 
  * 
  * Copyright (C) 1998 - 2020, Daniel Stenberg, <daniel@haxx.se>, et al.
  * 
  * Dependency: libcurl (sudo apt install libcurl4-nss-dev)
  * 
  */
  
#include <stdio.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <inttypes.h> 
#include <stdlib.h>
#include <time.h>
#include "log.h"
#include "ini.h"

#define RECEIVER 0
#define TRANSMITTER 1

#define URLBUF_SIZE 200
#define FILESAVAILABLE_BUFSIZE 10000
#define FILENAME_LENGHT		500
#define FILECOUNTMAXIMUM	5000
#define FILEDIR_MAX_LEN		100
#define FILEDIR_DOWNLOAD_LEN	1000

int g_dircount=0;
int g_entrycount=0;
char input_filename[FILECOUNTMAXIMUM][FILENAME_LENGHT];
char input_directoryname[FILECOUNTMAXIMUM][FILENAME_LENGHT];
char *ini_file = NULL;

char *time_stamp(){
	char *timestamp = (char *)malloc(sizeof(char) * 18);
	memset(timestamp,0,18);
	time_t ltime;
	ltime=time(NULL);
	struct tm *tm;
	tm=localtime(&ltime);
	sprintf(timestamp,"%04d%02d%02d_%02d%02d%02d", tm->tm_year+1900, tm->tm_mon, 
		tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	return timestamp;
}


void listdirectory(const char *name, int indent)
{
    DIR *dir;
    struct dirent *direntry;
    if (!(dir = opendir(name)))
        return;
    while ((direntry = readdir(dir) ) != NULL ) { 
		char path[1024];
        if (direntry->d_type == DT_DIR) {
            if (strcmp(direntry->d_name, ".") == 0 || strcmp(direntry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, direntry->d_name);
            sprintf(input_directoryname[g_dircount],"%s",path); 
            g_dircount++;
            listdirectory(path, indent + 2);
        } else {
        }   
    }
    closedir(dir);
}

void listfilesindirectory(const char *name, int indent)
{
    DIR *dir;
    struct dirent *fileentry;
    if (!(dir = opendir(name)))
        return;
    while ((fileentry = readdir(dir) ) != NULL ) {
        if (fileentry->d_type == DT_DIR) {
            
        } else {
			sprintf(input_filename[g_entrycount++],"%s/%s",name,fileentry->d_name);
        }   
    }
    closedir(dir);
}


/* strlen is evil */
void remove_new_line(char* string)
{
    size_t length = strlen(string);
    if((length > 0) && (string[length-1] == '\n'))
    {
        string[length-2] ='\0';
    }
}

/*static size_t filedownloadcallback(void *contents, size_t size, size_t nmemb, void *userp)
{ 
    size_t realsize = size * nmemb;
    log_info("[%d] download data callback received: %d %s ", getpid(),realsize,contents );
    return realsize;
}*/

static size_t fileuploadcallback(void *contents, size_t size, size_t nmemb, void *userp)
{ 
    size_t realsize = size * nmemb;
    remove_new_line(contents);
    // log_info("[%d] upload data received: (%d) %s ", getpid(),realsize,contents );
    return realsize;
}

/* HTTP post file to server */
int fileupload(char *uploadurl, char *filename)
{
	CURL *curl;
	CURLcode res;
	curl_mime *form = NULL;
	curl_mimepart *field = NULL;
	struct curl_slist *headerlist = NULL;
	static const char buf[] = "Expect:"; 
	// log_info("[%d] upload '%s' to URL: %s ", getpid(), filename, uploadurl);
	
	curl = curl_easy_init();  
	ini_t *config = ini_load(ini_file);
	const char *useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0";
	ini_sget(config, "http", "useragent", NULL, &useragent);
	if(curl) {
		form = curl_mime_init(curl);
		field = curl_mime_addpart(form);
		curl_mime_name(field, "sendfile");
		curl_mime_filedata(field, filename);
		field = curl_mime_addpart(form);
		curl_mime_name(field, "filename");
		curl_mime_data(field, filename, CURL_ZERO_TERMINATED);
		field = curl_mime_addpart(form);
		curl_mime_name(field, "submit");
		curl_mime_data(field, "send", CURL_ZERO_TERMINATED);
		headerlist = curl_slist_append(headerlist, buf);    
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fileuploadcallback);
		curl_easy_setopt(curl, CURLOPT_URL, uploadurl);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, useragent);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
		curl_easy_setopt(curl, CURLOPT_CAINFO, NULL);
		curl_easy_setopt(curl, CURLOPT_MIMEPOST, form);
		res = curl_easy_perform(curl);
		if(res != CURLE_OK)
			log_error("[%d] curl_easy_perform() failed: %s", getpid(),curl_easy_strerror(res) );
		curl_easy_cleanup(curl);
		curl_mime_free(form);
		curl_slist_free_all(headerlist);
		if ( res == CURLE_OK )
			return 0;
	}
	return -1;  
}
		
int filedownload(char *downloadurl, char *filename)
{
	CURL *curl;
	CURLcode res;
	ini_t *config = ini_load(ini_file);
	const char *useragent;
	ini_sget(config, "http", "useragent", NULL, &useragent);
	
	curl = curl_easy_init();
	
	if( curl ) {
		
		curl_easy_setopt(curl, CURLOPT_URL, downloadurl);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, useragent);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
		curl_easy_setopt(curl, CURLOPT_CAINFO, NULL);
	
		FILE *f = fopen(filename, "wb");
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, f); 	
		res = curl_easy_perform(curl); 
		curl_easy_cleanup(curl); 
		fclose(f);
		
		if(res != CURLE_OK) {
			log_error("[%d] curl_easy_perform() failed: %s", getpid(),curl_easy_strerror(res) );
			
		} else {
			// log_trace("[%d] res: %d ", getpid(),res );
		}
	}
	return 0;
}

long int fsize(FILE *fp){
    long int sz, prev;   
    prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    sz=ftell(fp);
    fseek(fp,prev,SEEK_SET); 
    return sz;
}

int main(int argc, char *argv[])
{
	long int file_len=0;
	FILE *in_fp;
	char *in_directory = "";	
	char *target_directory = "";	
	char *downloaddirectory = "";
	char *deletefromserverurl = "";
	char *listserverfilesurl = "";
	char *downloadfileurl = "";
	char *uploadfileurl = "";
	
	
	/* Command line options */
	int c, role=0;
	while ((c = getopt (argc, argv, "rthi:")) != -1)
	switch (c)
	{
	case 'i':
		ini_file = optarg;
		break;
		
	case 'r':
		role=RECEIVER;
		log_info("[%d] httpcomm - receiver v0.1", getpid());
		break;
	case 't':
	    role=TRANSMITTER;
	    log_info("[%d] httpcomm - transmitter v0.1", getpid());
	    break;
	case 'h':
		log_info("[%d] httpcomm",getpid());
		log_info("[%d] Usage: -i [ini_file] -r receive mode (poll server directory) [DEFAULT MODE]",getpid());
		log_info("[%d] -i [ini_file] -t transmit mode (poll local directory)",getpid());
		return 1;
	break;
		default:
		break;
	}
	
	if (ini_file == NULL) 
	{
		log_error("[%d] ini file not specified, exiting. ", getpid());
		return 0;
	}
	
	ini_t *config = ini_load(ini_file);
	/* read ini-file */
	ini_sget(config, "http", "inputdirectory", NULL, &in_directory);			// TRANSMITTER: Where files are uploaded from 
	ini_sget(config, "http", "downloaddirectory", NULL, &downloaddirectory); 	// RECEIVER: Where files are downloaded
	ini_sget(config, "http", "targetdirectory", NULL, &target_directory); 		// RECEIVER: Where files are moved after download	
	ini_sget(config, "http", "listfilesurl", NULL, &listserverfilesurl);
	ini_sget(config, "http", "deleteurl", NULL, &deletefromserverurl); 
	ini_sget(config, "http", "downloadfileurl", NULL, &downloadfileurl);
	ini_sget(config, "http", "uploadfileurl", NULL, &uploadfileurl);
	/* For sanity reasons, log only options valid to mode */
	if ( role == TRANSMITTER ) 
		log_trace("[%d] Server upload URL [TO] (%s)",getpid(),uploadfileurl);
	
	if ( role == RECEIVER ) 
	{
		log_trace("[%d] Server list files URL (%s) ",getpid(),listserverfilesurl);
		log_trace("[%d] Server download URL (%s) ",getpid(),downloadfileurl);
		log_trace("[%d] Server delete URL (%s) ",getpid(),deletefromserverurl);
	}
	/* Create directories if they do not exist */
	struct stat st = {0};
	if (stat(in_directory, &st) == -1) {
		log_trace("[%d] Creating directory (%s) ",getpid(),in_directory);
		mkdir(in_directory, 0755);
	}
	if (stat(downloaddirectory, &st) == -1) {
		log_trace("[%d] Creating directory (%s) ",getpid(),downloaddirectory);
		mkdir(downloaddirectory, 0755);
	}
	if (stat(target_directory, &st) == -1) {
		log_trace("[%d] Creating directory (%s) ",getpid(),target_directory);
		mkdir(target_directory, 0755);
	}
	
	/* Init CURL */ 
	if ( curl_global_init(CURL_GLOBAL_ALL) != 0 ) {
		log_error("[%d] Global curl init failed! Exiting. ",getpid());
		exit(0);
	}
	
	/* Transmitter */
	while ( role == TRANSMITTER )
	{
		/* Monitor input directory for file uploads */
		for (int loop=0; loop<FILECOUNTMAXIMUM;loop++)
		{
			memset(input_filename[loop],0,FILENAME_LENGHT);
			memset(input_directoryname[loop],0,FILENAME_LENGHT);
		}
		g_entrycount=0;
		listfilesindirectory(in_directory,0);
		for (int loop=0; loop < g_entrycount; loop++)
		{ 
				/* Size of File to upload */
				in_fp  = fopen(input_filename[loop], "rb");
				file_len = fsize(in_fp);
				log_trace("[%d] File: %s Size: %ld ",getpid(),input_filename[loop],file_len); 
				fclose(in_fp);
				
			if ( fileupload(uploadfileurl,input_filename[loop]) == 0 ) {
				log_trace("[%d] Uploaded file (%s)",getpid(),input_filename[loop] );
				if ( remove(input_filename[loop]) == 0 ) {
					log_trace("[%d] Removed file (%s) ",getpid(),input_filename[loop] );
				} else {
					log_error("[%d] Failed to remove file  (%s).",getpid(),input_filename[loop] );
				}
				log_trace("[%d] ************* ",getpid() );
			}
			else {
				log_error("[%d] File upload failed (%s)",getpid(),input_filename[loop] );
			}
		}
		sleep(1);
	}
	
	/* receiver */
	while ( role == RECEIVER  )
	{
		/* List files on remote server to "/tmp/files_available" */
		filedownload(listserverfilesurl, "/tmp/files_available");
		/* Parse result */
		const char filename[] = "/tmp/files_available";
		
		FILE *file = fopen(filename, "r");
		if ( file )
		{
			char buffer[FILESAVAILABLE_BUFSIZE];
			char downloadurlbuf[URLBUF_SIZE];
			char deleteurlbuffer[URLBUF_SIZE];
			char localfile[FILENAME_LENGHT];					// download (path/filename)
			char targetfileanddirectory[FILEDIR_DOWNLOAD_LEN];	// target (path/filename)
			char filename[FILENAME_LENGHT];						// filename 		
			memset(buffer,0,FILESAVAILABLE_BUFSIZE);
			memset(downloadurlbuf,0,URLBUF_SIZE);
			memset(deleteurlbuffer,0,URLBUF_SIZE);
			memset(localfile,0,FILENAME_LENGHT);
			memset(targetfileanddirectory,0,FILEDIR_DOWNLOAD_LEN);
			memset(filename,0,FILENAME_LENGHT);

			fgets(buffer, sizeof buffer, file); 
			int init_size = strlen(buffer); 
			
			if ( init_size > 0 ) 
			{
				log_trace("[%d] Server response: %s (%d)",getpid(),buffer,init_size);
				char delim[] = ",";
				char *ptr = strtok(buffer, delim);
				while(ptr != NULL)
				{
					sprintf(downloadurlbuf,"%s/%s",downloadfileurl,ptr);		
					sprintf(localfile,"%s/%s",downloaddirectory,ptr);			
					sprintf(deleteurlbuffer,"%s%s",deletefromserverurl,ptr);	
					sprintf(filename,"%s",ptr);									
					log_trace("[%d] Downloading (%s).",getpid(),downloadurlbuf); 
					filedownload(downloadurlbuf, localfile); 
					log_trace("[%d] Received file (%s). ",getpid(),localfile); 	
					sprintf(targetfileanddirectory,"%s/%s",target_directory,filename);
					log_info("[%d] Moving to target directory: %s", getpid(),targetfileanddirectory);
					rename(localfile, targetfileanddirectory);		
					// Call delete file url with pseudo filename for reply storage
					log_trace("[%d] Deleting from server (%s).",getpid(),deleteurlbuffer);
					filedownload(deleteurlbuffer, "/tmp/delete");
					log_trace("[%d] ************* ",getpid() );
					// keep on parsing
					ptr = strtok(NULL, delim);
				}
			}
			fclose(file);
		}
		sleep (1);
	}
	
	return 0;
	
	
}








